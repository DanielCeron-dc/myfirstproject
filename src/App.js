
import './App.css';
import Loader from './components/Loader/Loader';

function App() {
  return (
    <div className="App">
      <h1>This is an app to test gitpod :D</h1>
      <Loader/>
    </div>
  );
}

export default App;
 